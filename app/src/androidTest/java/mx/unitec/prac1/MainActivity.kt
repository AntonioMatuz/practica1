package mx.unitec.prac1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

val TAG =getString(R.string.debug)

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i(TAG, msg: "onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, msg: "onStar")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i(TAG, msg: "onRestar")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, msg: "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, msg: "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, msg: "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, msg: "onDestroy")
    }


}